﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpDownBehavior : MonoBehaviour
{

    public Direction direction;

   // public Direction direct;

    [SerializeField]
    float speed = 10f;

    private void Awake()
    {
       // direction = direct;
    }
    // Update is called once per frame
    void Update()
    {
        if(direction == Direction.up)
        {
            this.transform.Translate(Vector3.up * Time.deltaTime * speed);
        }
        else if(direction==Direction.down)
        {
            this.transform.Translate(Vector3.down * Time.deltaTime * speed);
        }
        else if (direction == Direction.left)
        {
            this.transform.Translate(Vector3.left * Time.deltaTime * speed);
        }
        else if (direction == Direction.right)
        {
            this.transform.Translate(Vector3.right * Time.deltaTime * speed);
        }
    }

    
}

public enum Direction
{
    up,
    down,
    left,
    right
}
