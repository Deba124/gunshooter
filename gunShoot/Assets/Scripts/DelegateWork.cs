﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.Events;

public class DelegateWork : MonoBehaviour
{
    public GameObject[] boxes = new GameObject[2];

    public UnityEngine.UI.Button btn;

    public Material red, green;

    delegate void ColorChanger();

    ColorChanger changer;

    UnityEvent caller;

    private void Start()
    {
        changer += ChangeToRed;
        changer += ChangeToGreen;

        caller = new UnityEvent();

        caller.AddListener(ChangeToRed);
        caller.AddListener(ChangeToGreen);
        caller.AddListener(PrintingData);

        btn.onClick.AddListener(delegate
        {
            caller.Invoke();
        });
        
    }

    IEnumerator crt()
    {
        yield return func();
        print("");

        boxes[0].transform.localRotation = Quaternion.Euler( new Vector3(12f, 4f, 0f));
        boxes[0].transform.localScale = new Vector3(1f, 1f, 1f);
    }

    object func()
    {
        return 0;
    }

    public void ChangeColorNow()
    {
        
    }

    public void ChangeToRed()
    {
        boxes[0].GetComponent<Renderer>().material = red;
        boxes[0].GetComponent<Rigidbody>().useGravity = true;

    }

    public void ChangeToGreen()
    {
        boxes[1].GetComponent<Renderer>().material = green;
        boxes[1].GetComponent<Rigidbody>().useGravity = true;
    }

    public void PrintingData()
    {
        print("Printing data");
        return;
    }
}
