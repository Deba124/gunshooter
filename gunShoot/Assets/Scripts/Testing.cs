﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Testing : MonoBehaviour
{
    public GameObject sphere;
    float speed = 10000f;

    delegate void MyDelegate();

   static MyDelegate del;

    private void Start()
    {
        sphere.GetComponent<Rigidbody>().AddForce(Camera.main.transform.forward * speed);
        del += ShowMsg;
        del += ShowNumber;

        del();

        del -= ShowMsg;
        del -= ShowNumber;
    }
    private void Update()
    {
        
        
            //print("Hitting");
            
        
    }

    public void ShowMsg()
    {
        print("message");
    }

    public void ShowNumber()
    {
        print("number");
    }

    private void OnCollisionEnter(Collision collision)
    {
        //Debug.LogError(collision.gameObject.tag);
        //if(collision.gameObject.tag.Equals("Baloons"))
        //{
        //    Debug.LogError("Collided");
        //    Destroy(collision.gameObject);
        //}

       
        Destroy(this.gameObject);
    }

}
