﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;

public class ApiFire : MonoBehaviour
{
    const string URI = "http://gamersgram.worksamples.info/api/register";

    public InputField Name, Username, Email, Phone;
    public void RegisterNow()
    {
        StartCoroutine(RegisterUsingApi());
    }
    IEnumerator RegisterUsingApi()
    {


        WWWForm form = new WWWForm();

        form.AddField("name", Name.text);
        form.AddField("username", Username.text);
        form.AddField("email", Email.text);
        form.AddField("phone", Phone.text);

        UnityWebRequest request = UnityWebRequest.Post(URI, form);
        request.SetRequestHeader("app-auth", "gg_ludo_8080");
        yield return request.SendWebRequest();

        // Debug.LogError(request.downloadHandler.text);

        JsonParsed parsed = JsonUtility.FromJson<JsonParsed>(request.downloadHandler.text);

        Debug.LogError(parsed.response_data.name);
    }
}
[System.Serializable]
public class  JsonParsed
{
    public string response_code;
    public string response_msg;
    public ResponseDataPased response_data;
}
[System.Serializable]
public class ResponseDataPased
{
    public string name;
    public string userName;
}
