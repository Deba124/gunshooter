﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GetDamage : MonoBehaviour
{
    //public Text baloonExploded;

    public static GetDamage instance;

    private void Awake()
    {
        instance = this;
    }
    private void OnCollisionEnter(Collision collision)
    {
        //Debug.LogError(collision.gameObject.tag);
        if(collision.gameObject.tag.Equals("Bullet"))
        {
            //Debug.LogError("Collided with bullet");
            LevelManager.BallonExploded++;
            //baloonExploded.text = "Baloon exploded : "+ LevelManager.BallonExploded.ToString();
            //LevelManager.Bulletgiven--;
            Debug.LogError("exploaded: " + LevelManager.BallonExploded);

            Destroy(collision.gameObject);
            Destroy(this.gameObject);
        }

        if (collision.gameObject.tag.Equals("Down"))
        {
            //Debug.LogError("Collided with down");
           this.GetComponent<UpDownBehavior>(). direction = Direction.up;
        }
        else if (collision.gameObject.tag.Equals("Up"))
        {
            //Debug.LogError("Collided with up");
            this.GetComponent<UpDownBehavior>().direction = Direction.down;
        }
        else if (collision.gameObject.tag.Equals("Left"))
        {
            //Debug.LogError("Collided with up");
            this.GetComponent<UpDownBehavior>().direction = Direction.right;
        }
        else if (collision.gameObject.tag.Equals("Right"))
        {
            //Debug.LogError("Collided with up");
            this.GetComponent<UpDownBehavior>().direction = Direction.left;
        }
    }

    //Note this script
}
