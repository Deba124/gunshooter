﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelManager : MonoBehaviour
{
    public static int LevelCoverCount;
    public static int BallonExploded;
    public int TotalTime = 0;
    public static int Bulletgiven;

    bool invoked = false;

    public GameObject[] levels;

    public static int LevelCount = 0;

    public GameObject PrevLevel = null;

    public static LevelManager instance;

    public Text baloonExploded;

    private void Start()
    {
        instance = this;
        ProceedtoNextLevel();
    }

    private void Update()
    {
        baloonExploded.text = "Baloon exploded : "+ BallonExploded;
        WinLooseCheck();
    }

    public void WinLooseCheck()
    {

        

        

            if (Bulletgiven == 0 && LevelCoverCount > BallonExploded)
            {
                ReloadLevel();
            }
        
        
            if (LevelCoverCount == BallonExploded)
            {
                Debug.LogError("Proceeding to next");
                ProceedtoNextLevel();
            }
            //else
            //{
            //    Debug.LogError("Proceeding to prev");
            //    ReloadLevel();
            //}
        

        
    }

    
    public void SetUpLevel(int coverCount,int bulletGiven)
    {
        LevelCoverCount = coverCount;
        Bulletgiven = bulletGiven;
        BallonExploded = 0;

        BulletFire.instance.bulletCount.text = "Bullet left : " + LevelManager.Bulletgiven;
        
    }

    void ProceedtoNextLevel()
    {
        if (PrevLevel != null)
        {
            Destroy(PrevLevel);
        }

        GameObject level = Instantiate(levels[LevelCount]);
        level.transform.localPosition = new Vector3(0f, 8f, 16f);
        PrevLevel = level;
        SetUpLevel(level.GetComponent<LevelInformation>().coverCount, level.GetComponent<LevelInformation>().BulletGiven);
        LevelCount++;
    }

    void ReloadLevel()
    {
        LevelCount--;
        if (PrevLevel != null)
        {
            Destroy(PrevLevel);
        }

        GameObject level = Instantiate(levels[LevelCount]);
        level.transform.localPosition = new Vector3(0f, 8f, 16f);
        PrevLevel = level;
        SetUpLevel(level.GetComponent<LevelInformation>().coverCount, level.GetComponent<LevelInformation>().BulletGiven);
        LevelCount++;
    }
}
