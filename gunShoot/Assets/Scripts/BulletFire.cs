﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BulletFire : MonoBehaviour
{
    public GameObject bullet;
    public Transform firePosition;
    public ParticleSystem muzzleFlash;
    public GameObject Gun;
    public Transform recoilMod;
    public float maxrecoil = -20f,recoilSpeed = 10f,recoil = 0f;
    public bool recoilAllowed;

    public Text bulletCount;

    public static BulletFire instance;

    private void Awake()
    {
        instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
       // instance = this;
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetMouseButtonDown(0))
        {
            if(LevelManager.Bulletgiven <= 0)
            {
                return;
            }


            Invoke("DecrementBullet", 1f);
            muzzleFlash.Play();
            recoil += 0.1f;
            GameObject blt = Instantiate(bullet);
            blt.transform.position = firePosition.position;
        }
        if(recoilAllowed)
        Recoil();
    }

    void DecrementBullet()
    {
        LevelManager.Bulletgiven--;
        bulletCount.text = "Bullet left : " + LevelManager.Bulletgiven;
    }

    void CheckCount()
    {
        LevelManager.instance.WinLooseCheck();
    }

    //Note this script as well

    void Recoil()
    {
        if(recoil > 0)
        {
            var maxRecoil = Quaternion.Euler(maxrecoil, 0, 0);
           recoilMod.rotation = Quaternion.Slerp(recoilMod.rotation, maxRecoil, Time.deltaTime * recoilSpeed);
            Gun.transform.localEulerAngles = new Vector3( recoilMod.localEulerAngles.x,0f,0f);
            recoil -= Time.deltaTime;

        }
        else
        {
            recoil = 0;
            var minRecoil = Quaternion.Euler(Gun.transform.rotation.x, Gun.transform.rotation.y, Gun.transform.rotation.z);
            recoilMod.rotation = Quaternion.Slerp(recoilMod.rotation, minRecoil, Time.deltaTime * recoilSpeed);
            Gun.transform.localEulerAngles = new Vector3(recoilMod.localEulerAngles.x, 0f, 0f);
        }
    }
}
