﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class C 
{
    A a = new A();
    B b = new B();
    A k = new B();
    IData i = new B();

    public void getNum()
    {
        b.t = 9;
        k.s = 10;
        i.mdata();
    }
}
public interface IData
{
    void mdata();
}

