﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Examples : MonoBehaviour
{
    public int Setval { get; private set; }

    public static Examples instance;

    // Start is called before the first frame update
    void Start()
    {
        
        instance = this;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void func()
    {
        Setval = 9;
    }
}
