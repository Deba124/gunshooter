﻿using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

namespace Tests
{
    public class Gen
    {
        // A Test behaves as an ordinary method
        [Test]
        public void GenSimplePasses()
        {

            GameObject obj = new GameObject("escape");

            Assert.Throws<MissingComponentException>(()=> {
                obj.GetComponent<Rigidbody>().velocity = new Vector3(1f, 0f, 0f);
            });

            //obj.GetComponent<Rigidbody>().velocity = new Vector3(1f, 0f, 0f);
        }

        // A UnityTest behaves like a coroutine in Play Mode. In Edit Mode you can use
        // `yield return null;` to skip a frame.
        [UnityTest]
        public IEnumerator GenWithEnumeratorPasses()
        {
            // Use the Assert class to test conditions.
            // Use yield to skip a frame.
            yield return null;
        }
    }
}
